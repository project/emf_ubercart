This module extends Email Marketing Framework to easily allow Ubercart customers to sign up
for newsletters at checkout.

To install:
1. Enable EMF and sync lists
2. Enable EMF Ubercart
3. Go to Site Configuration -> Email Marketing Framework -> Ubercart and select which lists
   you want available for signup at checkout, as well other option
4. Enable the "Newsletter Subscriptions" checkout pane for Ubercart

Customers will see a checkout pane asking them to select which newsletters they would
like to sign up for. Once a customer completes checkout, their email will be added to
EMF to sync to the lists. The "Action Name" for each newsletter is used as the label
for each checkbox for signup.

